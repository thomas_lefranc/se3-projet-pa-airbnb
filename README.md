# Projet de Programmation Avancée · AirBnB New York

## Objectifs

Ce projet a pour objectif de mettre en application les concepts vus en
algorithmique et programmation cette année pour stocker et traiter efficacement
un gros volume de données. De manière plus spécifique, ce projet vous permet de
vous évaluer sur les points suivants:

- algorithmique et conception
- programmation en C
- structures des données
- gestion des entrées / sorties
- qualité de code (conventions, documentation, analyse statique)
- automatisation de la compilation
- travail en équipe avec outil de gestionnaire de versions
- [optionnel] tests unitaires et intégration continue

## Cahier des charges

### Contexte

AirBnB a mis à disposition les données des logements disponibles à New York en
2019. Ces données sont compilés dans un fichier `CSV`[^1] qui contient 32889
lignes: la première ligne contient la description des différentes colonnes et
ensuite chaque ligne correspondent à un logement. Voici le détail des différentes
colonnes:

```rtf
id: (entier) identifiant du logement
name: (chaîne de caractères) titre de l'annonce
host_id: (entier) identifiant de l'hôte
host_name: (chaîne de caractères) nom de l'hôte
neighbourhood_group: (chaîne de caractères) arrondissement de New York
neighbourhood: (chaîne de caractères) quartier de New York
latitude: (réel) latitude
longitude: (réel) longitude
room_type: (chaîne de caractères) type du logement
price: (entier) prix du logement par nuit
minimum_nights: (entier) nombre de nuits minimum
number_of_reviews: (entier) nombre d'avis
last_review: (date AAAA-MM-JJ) date du dernier avis
reviews_per_month: (réel) nombre d'avis par mois
calculated_host_listings_count: (entier) nombre de fois où le logement a été loué
availability_365: (entier) nombre de disponibilités sur l'année
```

Pour plus de détails sur les données, vous pouvez consulter l'annexe

[^1]:[Comma-Separated Values](https://fr.wikipedia.org/wiki/Comma-separated_values), voir le TP9

### Expression fonctionnelle du besoin

#### Logiciel de traitement de données AirBnB

Le programme demandé devra obligatoirement être réalisé en langage `C` et
devra pouvoir être capable de charger le fichier de données et de traiter des
requêtes utilisateurs. De plus le programme devra proposer deux modes de
fonctionnement:

- un mode interactif où l’utilisateur spécifie les requêtes
- un mode batch où le programme lit puis exécute toutes les requêtes d’un
fichier texte fourni en paramètre.

En supposant que le programme s’appelle `projet`, le mode interactif sera lancé via:

```bash
./projet fichier.csv
```

où `fichier.csv` est un fichier contenant des données au format mentionné
précédemment. Sur ce dépôt vous est fourni un fichier `airbnb_ny_2019.csv`.

Le mode batch pourra s’activer avec:

```bash
./projet -b fichier.csv fichier.txt
```

où `fichier.txt` contient la liste des requêtes à utiliser et l'option `-b`
permettra d'activer le mode batch. Votre programme devra avoir le
comportement suivant:

1. charger le fichier de données
1. attendre une commande
1. traiter la commande
1. afficher le résultat de cette commande et retourner à l’étape 2 jusqu’à ce
que `EOF` (fin de fichier ou `CTRL+D` soit reçu).

#### Requêtes demandées

Les requêtes seront __toutes__ de la forme suivante:

```bash
zone [operation seuil]*
```

où:

- `zone` peut prendre la valeur d'un quartier de New York, d'un arrondissement
de New York ou `global` pour rechercher dans tout New York. On prendra soin dans
le cas d'un nom avec espace de bien ajouter des guillements double (`"`) en
début et fin de nom.
- `operation` peut prendre les valeurs:
  - `price_max` : prix maximum par nuit à ne pas dépasser
  - `price_min` : prix minimum par nuit
  - `reviews_min` : nombre minimum de reviews à avoir
  - `nights_min` : nombre minimum de nuits à passer dans le logement
  - `availability_min` : nombre minimum de disponibilités sur l'année
  - `count_min` : nombre minimum de fois où le logement a été loué
- la syntaxe `[operation seuil]*` indique qu'il est possible de chaîner
  plusieurs critères de sélection mais également de n'en spécifier aucune.
  Dans ce dernier cas, il faut lister tous les logements d'une zone donnée.

Voici quelques exemples de requêtes valides:

- `Manhattan`: recherche de tous les logements de Manhattan sans aucun critère
  de recherche.
- `global price_max 500`: recherche dans tout NY de logements où le prix par
nuit est inférieur à 500 dollars.
- `Bronx reviews_min 5`: recherche de logements dans l'arrondissement du Bronx
où le nombre d'avis doit être supérieur à 5.
- `"Staten Island" price_min 200 price_max 1000`: recherche de logements dans
l'arrondissement de Staten Island où le prix par nuit doit être compris entre
200 et 1000 dollars.
- `"Upper East Side" availability_min 30 nights_min 5 price_max 800`: recherche
de logements dans le quartier de l'Upper East Side (arrondissement de Manhattan)
où le logement est disponible au moins 30 nuits par an, où le nombre minimum de
nuits à passer dans le logement est 5 et où le prix maximum par nuit est de 800
dollars.

_Note:_ ce format de requête implique qu'il n'est pas possible de faire une
recherche sur plusieurs zones en même temps.

#### Affichage et format de retour

En mode interactif, vous pouvez définir l'affichage qui vous semble le plus
lisible et pertinent. Par contre en mode batch (avec l'option `-b`), votre
programme ne doit pas fournir d'affichage parasite: il doit uniquement
afficher les logements qui correspond à la commande qui vient d'être définie.
Les logements doivent être affichés sous la forme suivante:

```rtf
id,host_id,neighbourhood_group,neighbourhood,price,minimum_nights,number_of_reviews,calculated_host_listings_count,availability_365
```

_Note:_ tout autre affichage que ceux demandés entraineront des pénalités.


Par exemple si vous faites la requête `Harlem price_max 30`, le résultat
doit être  identique (à l'ordre d'affichage près) à:

```rtf
6364324,33106693,Manhattan,Harlem,16,2,43,3,154
3359315,16948705,Manhattan,Harlem,22,10,3,1,0
17604613,29893675,Manhattan,Harlem,25,1,9,2,0
16557384,38853591,Manhattan,Harlem,25,2,102 1,99
21408195,145242566,Manhattan,Harlem,25,3,61,2,7
31071518,191442758,Manhattan,Harlem,28,4,6,1,0
8583175,28272799,Manhattan,Harlem,29,3,27,1,0
9786182,26627436,Manhattan,Harlem,29,28,42,1,16
17774989,121359320,Manhattan,Harlem,30,1,7,1,0
30408787,228332039,Manhattan,Harlem,30,1,3,1,8
21394087,145242566,Manhattan,Harlem,30,3,86,2,0
9866779,50680795,Manhattan,Harlem,30,8,1,1,0
```

#### Performance

Un des aspects qui sera évalué sera la performance de votre programme, en
particulier le nombre de requêtes traitées par seconde. À vous de choisir les
structures et algorithmes adaptés pour permettre un traitement efficace des
requêtes qui seront spécifiées.

_Note:_ l'évaluation de la performance de votre programme sera faite en
exécutant votre programme en mode batch (et en fournissant un fichier qui
contient 10000 requêtes aléatoires).

## Déliverables

Le déliverable attendu est un dépôt GitLab créé sur GitLab. Il sera inspecté au
plus tard le dimanche 29/05/2020 23:59 (CEST) et évalué en partie
automatiquement. Ce dépôt devra être accessible aux correcteurs et devra contenir:

- Le code source de votre programme dans le répertoire `src` pour les fichiers `*.c`
et `include` pour les fichiers `*.h`
- un fichier `Makefile` qui permettra de compiler automatiquement votre programme.
- un fichier `README.md` qui devra présenter brièvement votre programme (comment
le compiler, comment l’utiliser).
- un rapport au format `PDF` qui justifie les structures de données que vous
avez utilisés et qui explique les algorithmes principaux de votre programme.
- Merci de ne pas mettre le fichier `CSV` sur `GIT` pour ne pas encombrer
inutilement le serveur.

## Annexe

### Détails sur les données

- les arrondissements de New York sont: Bronx, Brooklyn, Manhattan, Queens, Staten
    Island
- les quartiers du Bronx sont: Allerton, Baychester, Belmont, Bronxdale, Castle
    Hill, City Island, Claremont Village, Clason Point, Co-op City, Concourse,
    Concourse Village, East Morrisania, Eastchester, Edenwald, Fieldston,
    Fordham, Highbridge, Hunts Point, Kingsbridge, Longwood, Melrose, Morris
    Heights, Morris Park, Morrisania, Mott Haven, Mount Eden, Mount Hope, North
    Riverdale, Norwood, Olinville, Parkchester, Pelham Bay, Pelham Gardens, Port
    Morris, Riverdale, Schuylerville, Soundview, Spuyten Duyvil, Throgs Neck,
    Tremont, Unionport, University Heights, Van Nest, Wakefield, West Farms,
    Westchester Square, Williamsbridge, Woodlawn
- les quartiers de Brooklyn sont: Bath Beach, Bay Ridge, Bedford-Stuyvesant,
    Bensonhurst, Bergen Beach, Boerum Hill, Borough Park, Brighton Beach,
    Brooklyn Heights, Brownsville, Bushwick, Canarsie, Carroll Gardens, Clinton
    Hill, Cobble Hill, Columbia St, Coney Island, Crown Heights, Cypress Hills,
    DUMBO, Downtown Brooklyn, Dyker Heights, East Flatbush, East New York,
    Flatbush, Flatlands, Fort Greene, Fort Hamilton, Gowanus, Gravesend,
    Greenpoint, Harlem, Kensington, Manhattan Beach, Midwood, Mill Basin, Navy
    Yard, Park Slope, Prospect Heights, Prospect-Lefferts Gardens, Red Hook, Sea
    Gate, Sheepshead Bay, South Slope, Sunset Park, Vinegar
    Hill, Williamsburg, Windsor Terrace
- les quartiers de Manhattan sont: Battery Park City, Chelsea, Chinatown, Civic
    Center, East Harlem, East Village, Financial District, Flatiron District,
    Gramercy, Greenwich Village, Harlem, Hell's Kitchen, Inwood, Kips Bay, Little
    Italy, Lower East Side, Marble Hill, Midtown, Morningside Heights, Murray
    Hill, NoHo, Nolita, Roosevelt Island, SoHo, Stuyvesant Town, Theater
    District, Tribeca, Two Bridges, Upper East Side, Upper West Side, Washington
    Heights, West Village
- les quarties du Queens sont: Arverne, Astoria, Bay Terrace, Bayside,
    Bayswater, Belle Harbor, Bellerose, Breezy Point, Briarwood, Cambria Heights,
    College Point, Corona, Ditmars Steinway, Douglaston, East Elmhurst, Edgemere,
    Elmhurst, Far Rockaway, Flushing, Forest Hills, Fresh Meadows, Glendale,
    Hollis, Holliswood, Howard Beach, Jackson Heights, Jamaica, Jamaica Estates,
    Jamaica Hills, Kew Gardens, Kew Gardens Hills, Laurelton, Little Neck, Long
    Island City, Maspeth, Middle Village, Neponsit, Ozone Park, Queens Village,
    Rego Park, Richmond Hill, Ridgewood, Rockaway Beach, Rosedale, South Ozone
    Park, Springfield Gardens, St. Albans, Sunnyside, Whitestone, Woodhaven,
    Woodside
- les quartiers de Staten Island: Arden Heights, Arrochar, Bull's Head,
    Castleton Corners, Clifton, Concord, Dongan Hills, Emerson Hill,
    Graniteville, Grant City, Great Kills, Grymes Hill, Howland Hook, Huguenot,
    Lighthouse Hill, Mariners Harbor, Midland Beach, New Brighton, New Dorp
    Beach, New Springville, Oakwood, Port Richmond, Prince's Bay, Randall Manor,
    Rosebank, Rossville, Shore Acres, Silver Lake, South Beach, St. George,
    Stapleton, Todt Hill, Tompkinsville, Tottenville, West Brighton, Westerleigh
- le type de logement est: Entire home/apt, Private room, Shared room

### Détails de l'évaluation

L’évaluation de ce projet sera faite en utilisant de manière intensive les
outils de traitement automatique. D’une part parce que ce sont des outils qui
sont utilisés dans le monde professionnel (et autant les maîtriser le plus tôt
possible) et d’autre part puisque cela permet d’évaluer objectivement une partie
de votre travail. Les points sur lesquels vous devez être vigilants:

- respect des consignes et du cahier des charges, notamment le format des
requêtes, l'affichage demandé...
- la pertinence de vos choix de structures de données et d'algorithmes en
fonction de l'utilisation qui a été définie dans le cahier de charges.
- la compilation automatique de votre projet avec Makefile et l'utilisation de
votre programme suivant les deux modes précisés
- la qualité de votre code à la fois sur les conventions de codage
(`clang-format`), sur les failles potentiels (`clang-tidy`), sur les
applications des principes `KISS` et `DRY` sur la mauvaise utilisation mémoire
(`valgrind`)...
- la qualité de votre dépôt `GIT`: commits explicites, dépôt sans fichiers
auxiliaires et sans le fichier `CSV`...
- la régularité de votre travail et votre travail en équipe: commits réguliers,
participation équitable des deux membres du binôme, absence de plagiat sur un
autre binôme (`moss`)

#### Liste des tests automatiques

Voici la liste des tests automatiques, vous constaterez que la majorité des
tests sont des malus (points déduits de la note finale) car ils correspondent
à des erreurs que vous ne devez pas commettre. À l'inverse ils existent
quelques tests qui ne sont pas demandés mais qui peuvent ajouter des points
bonus sur la note finale.

|Catégorie|Test|Commande|Valeur attendue|Points|
|---|---|---|---|---|
|Déliverables|Fichier README|`find . -name "README*" -maxdepth 1 \| wc -l`|1|0 si OK<br>-1 sinon|
|Déliverables|Ficher pdf|`find . -name "*.pdf" -maxdepth 6 \| wc -l`|1|0 si OK<br>-1 sinon|
|Déliverables|Fichier Makefile|`find . -name "Makefile" -maxdepth 1 \| wc -l`|1|0 si OK<br>-1 sinon|
|Déliverables|Fichiers `.c`|`find . -name "*.c" -maxdepth 1 \| wc -l`|0|0 si OK<br>-0.25 sinon|
|Déliverables|Fichiers `.c`|`find src -name "*.c" -maxdepth 6 \| wc -l`|>0|0 si OK<br>-0.25 sinon|
|Déliverables|Fichiers `.h`|`find . -name "*.h" -maxdepth 1 \| wc -l`|0|0 si OK<br>-0.25 sinon|
|Déliverables|Fichiers `.h`|`find include -name "*.h" -maxdepth 6 \| wc -l`|>0|0 si OK<br>-0.25 sinon|
|Dépôt GIT|Pas de fichiers aux|`find . -name PATTERN -maxdepth 6\| wc -l` [^2]|0|0 si OK<br>-0.25 par fichier (max -2) sinon|
|Dépôt GIT|Pas d'exécutables|`find . -type f -executable -not -path "./.git/*" -maxdepth 6\| wc -l`|0|0 si OK<br>-0.25 sinon|
|Dépôt GIT|Pas de données|`find . -name "*.csv" -maxdepth 6\| wc -l`|0|0 si OK<br>-1 sinon|
|Dépôt GIT|GIT ignore|`find . -name ".gitignore" -maxdepth 1\| wc -l`|1|+0.25 si OK<br>0 sinon|
|Dépôt GIT|.clang-format|`find . -name ".clang-format" -maxdepth 1\| wc -l`|1|+0.25 si OK<br>0 sinon|
|Dépôt GIT|Nombre de commits|`git rev-list --all --remotes \| wc -l`|>10|0 si OK<br>-0.25 par commits manquants sinon|
|Dépôt GIT|Nombre de branches|`git branch -r \| grep -v "HEAD" \| grep -E -o "[a-z0-9\-]*$"`|>1|+0.25 si OK<br>0 sinon|
|Dépôt GIT|Jours d'activité|`git log --format="%ad %H" --date=iso \| cut -d' ' -f1 \| sort \| uniq -c`|>5|+0.25 par jour sup (max +2) si OK<br>0 sinon|
|Dépôt GIT|Répartition du travail|`git summary --line`|diff < 40%|0 si OK<br>-2 sinon|
|Qualité Code|clang-format|`python3 $HOME/aux/run-clang-format.py -r . \| grep -c "+++"`|0|0 si OK<br>-0.25 sinon|
|Qualité Code|Doublons (DRY)|`jscpd -s -r csv -o . -m strict --pattern="*"`|0|0 si OK<br>-0.25 sinon|
|Qualité Code|clang-tidy|`clang-tidy --quiet -checks="cert,clang-analyzer,linuxkernel,llvm,llvm-libc,misc" *.c --`|0|0 si OK<br>-0.25 si warnings<br>-1 si erreurs|
|Makefile|debug|`make -n && grep -c -e "\-g"`|1|+0.25 si OK<br>0 sinon|
|Makefile|warnings|`make -n && grep -c -e "\-W*"`|1|0 si OK<br>-0.5 sinon|
|Makefile|optimisations|`make -n && grep -c -e "\-O."`|1|+0.25 si OK<br>0 sinon|
|Makefile|erreurs|`make \| grep -c "No rule to make target" < $TEMP_LOG`|0|0 si OK<br>-1 sinon|
|Compilation|erreurs|`make \| grep -c "error:"`|0|0 si OK<br>-2 sinon|
|Compilation|warnings|`make \| grep -c "warning:"`|0|0 si OK<br>-0.5 sinon|
|Exécution|code de retour|`./exec airbnb_ny_2019.csv sample.txt`|0|0 si OK -1 sinon|
|Exécution|temps|`time ./exec airbnb_ny_2019.csv 10k_requetes.txt`|le plus petit possible|+2 si top1<br>+1 si top5<br>0 sinon|
|Exécution|mémoire|`valgrind -q ./exec airbnb_ny_2019.csv 10k_requetes.txt`|0|0 si OK<br>-1 sinon|
|Plagiat|moss|`./moss.sh tmp/*/*/.c tmp/*/*/.h`|<10|0 si OK<br>0 au projet sinon|
|Intégration continue|Tests unitaires|||+2 si présents et pertinents|
|Intégration continue|.gitlab-ci.yml|||+3 si présent et pertinent|
|Divers|Correction du sujet|||[+0.5, +2] suivant la criticité de l'erreur trouvée|

[^2]: `PATTERN` peut prendre les valeurs suivantes: `*.d`, `*.o`, `*.lib`,
`*.a`, `*.dll`, `*.so`, `*.so.*`, `*.dylib`, `*.exe`, `*.out`, `*.dSYM/`,
`*.su`, `*.idb`, `*.pdb`, `*~`, `\#*\#`, `/.emacs.desktop`,
`/.emacs.desktop.lock`, `*.elc`, `auto-save-list`, `tramp`, `.\#*`, `[._]*.s
[a-v][a-z]`, `[._]*.sw[a-p]`, `[._]s[a-rt-v][a-z]`, `[._]ss[a-gi-z]`, `[._]sw
[a-p]`, `Session.vim`, `Sessionx.vim`, `tags`, `.vscode/*`,
`!.vscode/settings.json`, `!.vscode/tasks.json`, `!.vscode/launch.json`,
`!.vscode/extensions.json`, `!.vscode/*.code-snippets`, `[Dd]ebug/`,
`[Dd]ebugPublic/`, `[Rr]elease/`, `[Rr]eleases/`, `x64/`, `x86/`, `[Ww][Ii]
[Nn]32/`, `[Aa][Rr][Mm]/`, `[Aa][Rr][Mm]64/`, `bld/`, `[Bb]in/`, `[Oo]bj/`,
`[Ll]og/`, `[Ll]ogs/`, `.vs/`, `Generated\ Files/`, `[Tt]est[Rr]esult*/`,
`[Bb]uild[Ll]og.*`, `*.VisualState.xml`, `TestResult.xml`, `nunit-*.xml`,
`[Dd]ebugPS/`, `[Rr]eleasePS/`, `dlldata.c`, `*_i.c`, `*_p.c`, `*_h.h`,
`*.ilk`, `*.meta`, `*.obj`, `*.iobj`, `*.pch`, `*.pdb`, `*.ipdb`, `*.pgc`,
`*.pgd`, `*.rsp`, `*.sbr`, `*.tlb`, `*.tli`, `*.tlh`, `*.tmp`, `*.tmp_proj`,
`*_wpftmp.csproj`, `*.log`, `*.tlog`, `*.vspscc`, `*.vssscc`, `.builds`,
`*.pidb`, `*.svclog`, `*.scc`, `ipch/`, `*.aps`, `*.ncb`, `*.opendb`,
`*.opensdf`, `*.sdf`, `*.cachefile`, `*.VC.db`, `*.VC.VC.opendb`, `*.psess`,
`*.vsp`, `*.vspx`, `*.sap`
